#Taller

#Dada una lista de 1000 valores generados aleatoriamente entre 0 y 20
# 1. Crear una función que genere otra lista con valores TRUE o FALSE
# dependiendo si el valor de la lista es primo o no
# 2. Crear una función que retorne la media de la lista que se pase 
# 3. Crear una funcióin que devuuelva una lista con el factorial de cada uno de los números de la
#    lista que se pase como argumento
# 4. Crear una función que retorne el valor mayor y menor de una lista

import random
import math
import pandas as pd

def generar_lista(limite_inferior, limite_superior, cantidad):
    lista=[]
    for i in range(0, cantidad):
        valor=random.randrange(limite_inferior,limite_superior)
        lista.append(valor)
    return lista

def verificar_primo(numero):
    if numero>1:
        for i in range(2, numero):
            if (numero%i ==0):
                return False
        return True
    else:
        return False

def definir_primos(lista):
    lista_resultado=[]
    for i in lista:
        resultado=verificar_primo(i)
        lista_resultado.append(resultado)
    return lista_resultado

def calcular_media(lista):
    serie=pd.Series(lista)
    return serie.mean()


def definir_factorial(lista):
    lista_resultado=[]
    for i in lista:
        valor=math.factorial(i)
        lista_resultado.append(valor)
    return lista_resultado

def calcular_menor_mayor(lista):
    menor=min(lista)
    mayor=max(lista)
    return menor, mayor

miLista=generar_lista(0,20,10)
print(miLista)
print(calcular_menor_mayor(miLista))





